//
//  ViewController.swift
//  Car360UISDKSample
//
//  Created by Remy Cilia on 2/20/18.
//  Copyright © 2018 Car360. All rights reserved.
//

import UIKit
import Car360Core
import Car360UI
import MBProgressHUD

class ViewController: UIViewController {
    struct Constants {
        private init() {}
        
        static let username = "USERNAME"
        static let password = "PASSWORD"
    }
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var newCaptureButton: UIButton!
    @IBOutlet weak var openGalleryButton: UIButton!
    
    // MARK: - UIViewController methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUIFramework()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loginIfNeeded()
    }
    
    // MARK: - Car360 UI Framework methods
    
    func configureUIFramework() {
        Car360Configuration.Colors.backgroundColor = .darkGray
    }
    
    func loginIfNeeded() {
        if LocalUser.currentUser == nil {
            login()
        }
    }
    
    func login() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        Car360CoreFramework.login(username: Constants.username, password: Constants.password) { (error: APIError?) -> (Void) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            self.enableButtons(enable: error == nil)
            
            if let error = error {
                let alert = UIAlertController(title: "Login error", message: error.message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func openCapture() {
        do {
            let newCaptureVC = try Car360UI.getCaptureTypeViewController()
            self.navigationController?.pushViewController(newCaptureVC, animated: true)
        } catch Car360FrameworkError.userNotLoggedIn {
            print("no logged in user")
        } catch {
            print("unknown error")
        }
    }
    
    func openGallery() {
        if let vehicleManagementVC = try? Car360UI.getVehicleManagementViewController() {
            self.navigationController?.pushViewController(vehicleManagementVC, animated: true)
        }
    }
    
    // MARK: - UI Methods
    
    func enableButtons(enable: Bool) {
        self.newCaptureButton.isEnabled = enable
        self.openGalleryButton.isEnabled = enable
    }
    
    // MARK: - Actions
    
    @IBAction func newCaptureAction(_ sender: Any) {
        openCapture()
    }
    
    @IBAction func openGalleryAction(_ sender: Any) {
        openGallery()
    }
}

